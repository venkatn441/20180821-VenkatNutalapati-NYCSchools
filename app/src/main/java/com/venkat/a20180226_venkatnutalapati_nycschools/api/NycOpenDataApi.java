package com.venkat.a20180226_venkatnutalapati_nycschools.api;

import com.venkat.a20180226_venkatnutalapati_nycschools.model.College;
import com.venkat.a20180226_venkatnutalapati_nycschools.model.Scores;

import java.util.ArrayList;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by venkat on 08/20/18.
 */

public interface NycOpenDataApi {

    //To get the colleges list for given state
    @GET("97mf-9njv.json")
    Observable<ArrayList<College>> getCollegesList(@Query("state_code") String state);

    //To get the socres for the selected college
    @GET("734v-jeq5.json")
    Observable<ArrayList<Scores>> getScores(@Query("dbn") String dbn);
}
