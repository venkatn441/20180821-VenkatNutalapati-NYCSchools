package com.venkat.a20180226_venkatnutalapati_nycschools.util.rx;

import rx.Scheduler;

/**
 * Created by venkat on 08/20/18.
 */

public interface RxSchedulers {
    Scheduler io();
    Scheduler androidThread();
}
