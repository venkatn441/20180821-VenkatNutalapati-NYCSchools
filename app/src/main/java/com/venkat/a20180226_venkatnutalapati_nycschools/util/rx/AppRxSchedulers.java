package com.venkat.a20180226_venkatnutalapati_nycschools.util.rx;

import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by venkat on 08/20/18.
 */

public class AppRxSchedulers implements RxSchedulers {

    @Override
    public Scheduler io() {
        return Schedulers.io();
    }

    @Override
    public Scheduler androidThread() {
        return AndroidSchedulers.mainThread();
    }
}