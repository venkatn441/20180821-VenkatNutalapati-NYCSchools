package com.venkat.a20180226_venkatnutalapati_nycschools.application.builder;

import com.venkat.a20180226_venkatnutalapati_nycschools.api.NycOpenDataApi;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by venkat on 08/20/18.
 *
 * Provides a global retrofit instance
 */

@Module
public class ApiServiceModule {
    private static final String BASE_URL = "https://data.cityofnewyork.us/resource/";

    @AppScope
    @Provides
    NycOpenDataApi provideApiService(OkHttpClient client, GsonConverterFactory gson, RxJavaCallAdapterFactory rxAdapter) {
        Retrofit retrofit = new Retrofit.Builder().client(client)
                .baseUrl(BASE_URL).addConverterFactory(gson).
                        addCallAdapterFactory(rxAdapter).build();

        return retrofit.create(NycOpenDataApi.class);
    }
}
