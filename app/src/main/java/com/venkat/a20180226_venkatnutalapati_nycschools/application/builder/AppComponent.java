package com.venkat.a20180226_venkatnutalapati_nycschools.application.builder;

import com.venkat.a20180226_venkatnutalapati_nycschools.api.NycOpenDataApi;
import com.venkat.a20180226_venkatnutalapati_nycschools.util.rx.RxSchedulers;

import dagger.Component;

/**
 * Created by venkat on 08/20/18.
 */

@AppScope
@Component(modules = {NetworkModule.class, AppContextModule.class, ApiServiceModule.class, RxModule.class})
public interface AppComponent {

    RxSchedulers rxSchedulers();
    NycOpenDataApi apiService();
}
