package com.venkat.a20180226_venkatnutalapati_nycschools.application.builder;

import com.venkat.a20180226_venkatnutalapati_nycschools.util.rx.AppRxSchedulers;
import com.venkat.a20180226_venkatnutalapati_nycschools.util.rx.RxSchedulers;

import dagger.Module;
import dagger.Provides;

/**
 * Created by venkat on 08/20/18.
 */

@Module
public class RxModule {

    @Provides
    RxSchedulers provideRxSchedulers() {
        return new AppRxSchedulers();
    }
}
