package com.venkat.a20180226_venkatnutalapati_nycschools.application.builder;

import android.content.Context;

import java.io.File;
import java.util.concurrent.Executors;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.schedulers.Schedulers;

/**
 * Created by venkat on 08/20/18.
 *
 * This class provides all the dependencies to build a retrofit instance(ApiServiceModule)
 */

@Module
public class NetworkModule {

    //Provides Logging interceptor to retrofit for logging the network calls and results
    @AppScope
    @Provides
    OkHttpClient provideHttpClient(HttpLoggingInterceptor logger) {
        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.addInterceptor(logger);
        return builder.build();
    }

    //Interceptor for enabling logging retrofit calls adn respones.
    @AppScope
    @Provides
    HttpLoggingInterceptor provideInterceptor() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return httpLoggingInterceptor;
    }

    //Provides RxAdapter for use with retrofit
    @AppScope
    @Provides
    RxJavaCallAdapterFactory provideRxAdapter() {
        return RxJavaCallAdapterFactory.createWithScheduler(Schedulers.from(Executors.newCachedThreadPool()));
    }

    //Provides GsonConverterFactory to retrofit
    @AppScope
    @Provides
    GsonConverterFactory provideGsonClient() {
        return GsonConverterFactory.create();
    }

}
