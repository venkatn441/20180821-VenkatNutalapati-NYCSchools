package com.venkat.a20180226_venkatnutalapati_nycschools.application;

import android.app.Application;

import com.venkat.a20180226_venkatnutalapati_nycschools.application.builder.AppComponent;
import com.venkat.a20180226_venkatnutalapati_nycschools.application.builder.AppContextModule;
import com.venkat.a20180226_venkatnutalapati_nycschools.application.builder.DaggerAppComponent;

/**
 * Created by venkat on 08/20/18.
 */

public class AppController extends Application{

    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        initAppComponent();
    }

    private void initAppComponent() {
        appComponent = DaggerAppComponent.builder().appContextModule(new AppContextModule(this)).build();
    }

    public static AppComponent getNetComponent() {
        return appComponent;
    }
}
