package com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegeslist.core;

import com.venkat.a20180226_venkatnutalapati_nycschools.api.NycOpenDataApi;
import com.venkat.a20180226_venkatnutalapati_nycschools.model.College;
import com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegeslist.CollegeListActivity;
import com.venkat.a20180226_venkatnutalapati_nycschools.util.NetworkUtils;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by venkat on 08/20/18.
 *
 * This Model class which provides methods for network calls with a return type obeservable.
 */

public class CollegeModel {

    CollegeListActivity context;
    NycOpenDataApi api;

    public CollegeModel(CollegeListActivity context, NycOpenDataApi api) {
        this.context = context;
        this.api = api;
    }

    Observable<ArrayList<College>> getCollegesForState(String state) {
        return api.getCollegesList(state);
    }

    Observable<Boolean> isNetworkAvailable() {
        return NetworkUtils.isNetworkAvailableObservable(context);
    }

    public void goToScoresForCollege(String dbnCode) {
        context.goToScoresActivty(dbnCode);
    }
}
