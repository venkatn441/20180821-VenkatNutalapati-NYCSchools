package com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegeslist.dagger;

import com.venkat.a20180226_venkatnutalapati_nycschools.api.NycOpenDataApi;
import com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegeslist.CollegeListActivity;
import com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegeslist.core.CollegeModel;
import com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegeslist.core.CollegePresenter;
import com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegeslist.core.CollegeView;
import com.venkat.a20180226_venkatnutalapati_nycschools.util.rx.RxSchedulers;

import dagger.Module;
import dagger.Provides;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by venkat on 08/20/18.
 *
 * Module class provides all the required dependencies
 */

@Module
public class CollegeModule {
    CollegeListActivity context;

    public CollegeModule(CollegeListActivity context) {
        this.context = context;
    }

    @CollegeScope
    @Provides
    CollegeView provideview() {
        return new CollegeView(context);
    }

    @CollegeScope
    @Provides
    CollegePresenter providePresenter(CollegeView view, CollegeModel model, RxSchedulers scheduler) {
        CompositeSubscription subscriptions = new CompositeSubscription();
        return new CollegePresenter(view, model, scheduler, subscriptions);
    }

    @CollegeScope
    @Provides
    CollegeListActivity providesContext() {
        return context;
    }

    @CollegeScope
    @Provides
    CollegeModel provideModel(NycOpenDataApi api) {
        return new CollegeModel(context, api);
    }
}
