package com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegeslist.list;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.venkat.a20180226_venkatnutalapati_nycschools.R;
import com.venkat.a20180226_venkatnutalapati_nycschools.model.College;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.subjects.PublishSubject;

/**
 * Created by venkat on 08/20/18.
 */

public class CollegeViewHolder extends RecyclerView.ViewHolder{

    View view;

    @BindView(R.id.college_name)
    TextView dateTextView;
    @BindView(R.id.city_name)
    TextView high;
    @BindView(R.id.total_students)
    TextView low;


    public CollegeViewHolder(View itemView, PublishSubject<Integer> clickSubject) {
        super(itemView);
        view = itemView;
        ButterKnife.bind(this, view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("venkat", "Item Clicked>>>>>>>>>>>>>>>>>>>>>"+getAdapterPosition());
                clickSubject.onNext(getAdapterPosition());
            }
        });
        /*view.setOnClickListener(v -> clickSubject.onNext(getAdapterPosition()));*/
    }

    void bind(College collegeItem) {
        dateTextView.setText("School :"+collegeItem.getSchool_name().toString());
        high.setText("City :"+collegeItem.getCity().toString());
        low.setText("Students :"+collegeItem.getTotalstudents().toString());
    }
}
