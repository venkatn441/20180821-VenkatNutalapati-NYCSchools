package com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegeslist;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ProgressBar;

import com.venkat.a20180226_venkatnutalapati_nycschools.R;
import com.venkat.a20180226_venkatnutalapati_nycschools.application.AppController;
import com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegescore.ScoresActivity;
import com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegeslist.core.CollegePresenter;
import com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegeslist.core.CollegeView;
import com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegeslist.dagger.CollegeModule;
import com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegeslist.dagger.DaggerCollegeComponent;
import com.venkat.a20180226_venkatnutalapati_nycschools.util.Constants;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by venkat on 08/20/18.
 */

public class CollegeListActivity extends AppCompatActivity{
    //Injecting dependencies using dagger
    @Inject
    CollegeView view;
    @Inject
    CollegePresenter presenter;

    String state;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerCollegeComponent.builder().appComponent(AppController.getNetComponent()).
                collegeModule(new CollegeModule(this)).build().inject(this);

        setContentView(view.view());
        if(getIntent() != null) {
            state = getIntent().getStringExtra(Constants.STATE);
        }
        presenter.onCreate(state);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    public void goToScoresActivty(String dbnCode) {
        Intent in = new Intent(CollegeListActivity.this, ScoresActivity.class );
        in.putExtra("dbn",dbnCode);
        startActivity(in);
    }
}
