package com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegeslist.core;

import android.util.Log;
import android.view.View;

import com.venkat.a20180226_venkatnutalapati_nycschools.model.College;
import com.venkat.a20180226_venkatnutalapati_nycschools.util.rx.RxSchedulers;

import java.util.ArrayList;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.subjects.PublishSubject;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by venkat on 08/20/18.
 */

public class CollegePresenter {
    CollegeView view;
    CollegeModel model;
    CompositeSubscription subscriptions;
    RxSchedulers rxSchedulers;
    ArrayList<College> collegeList;

    private static final String TAG = "CollegePresenter";

    public CollegePresenter(CollegeView view, CollegeModel model, RxSchedulers schedulers, CompositeSubscription subscriptions) {
        this.view = view;
        this.model = model;
        this.rxSchedulers = schedulers;
        this.subscriptions = subscriptions;
    }

    public void onCreate(String state) {
        Log.d(TAG, "presenter oncreate");
        subscriptions.add(getCollegeList(state));
        subscriptions.add(respondToClick());
    }

    public void onDestroy() {
        subscriptions.clear();
    }

    //Subscribes for List item clicks
    private Subscription respondToClick() {
        return view.itemClicks().subscribe(integer -> {
            model.goToScoresForCollege(collegeList.get(integer).getDbn());
        });
    }


    // Checks for network availability and
    // API call is made to get list of colleges.
    // Network call is made on a background thread and list is upadted on UI thread
    // After the response is received list is added to recycler adapter.
    private Subscription getCollegeList(String state) {
        return model.isNetworkAvailable().doOnNext(isAvailable -> {
            if (!isAvailable) {
                Log.d(TAG, "no connection");
            }
        }).
                filter(isNetworkAvailable -> true).
                flatMap(isAvailable -> model.getCollegesForState(state)).
                subscribeOn(rxSchedulers.io()).observeOn(rxSchedulers.androidThread()).
                subscribe(collegesResponse -> {
                            //Hides the progress spinner
                            view.loading.setVisibility(View.GONE);
                            //Shows the recycler list
                            view.list.setVisibility(View.VISIBLE);
                            collegeList = collegesResponse;
                            view.swapAdapter(collegeList);
                        }, throwable -> {
                            Log.d(TAG, "request failed :" + throwable.toString());
                        }
                );
    }
}
