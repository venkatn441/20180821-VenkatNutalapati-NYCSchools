package com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegescore.dagger;

import com.venkat.a20180226_venkatnutalapati_nycschools.application.builder.AppComponent;
import com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegescore.ScoresActivity;

import dagger.Component;

/**
 * Created by venkat on 08/20/18.
 */

@ScoreScope
@Component(dependencies = {AppComponent.class} , modules = {ScoreModule.class})
public interface ScoreComponent {
    void inject (ScoresActivity activity);
}
