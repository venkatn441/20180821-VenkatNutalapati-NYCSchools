package com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegescore.dagger;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by venkat on 08/20/18.
 */

@Scope
@Retention(RetentionPolicy.CLASS)
@interface ScoreScope {
}
