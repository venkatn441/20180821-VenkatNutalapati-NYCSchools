package com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegeslist.list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.venkat.a20180226_venkatnutalapati_nycschools.R;
import com.venkat.a20180226_venkatnutalapati_nycschools.model.College;

import java.util.ArrayList;

import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by venkat on 08/20/18.
 */

public class CollegesAdapter extends RecyclerView.Adapter<CollegeViewHolder> {

    private final PublishSubject<Integer> itemClicks = PublishSubject.create();
    ArrayList<College> collegesArrayList = new ArrayList<>();

    public void swapAdapter(ArrayList<College> collegeArray) {
        collegesArrayList.clear();
        collegesArrayList.addAll(collegeArray);
        notifyDataSetChanged();
    }

    public Observable<Integer> observeClicks() {
        return itemClicks;
    }

    @Override
    public CollegeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.college_row_layout, parent, false);
        return new CollegeViewHolder(view ,itemClicks);
    }

    @Override
    public void onBindViewHolder(CollegeViewHolder holder, int position) {
        College college = collegesArrayList.get(position);
        holder.bind(college);
    }

    @Override
    public int getItemCount() {
        return collegesArrayList.size();
    }
}
