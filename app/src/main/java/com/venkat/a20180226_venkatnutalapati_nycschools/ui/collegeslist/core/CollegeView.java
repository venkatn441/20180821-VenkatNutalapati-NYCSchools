package com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegeslist.core;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.venkat.a20180226_venkatnutalapati_nycschools.R;
import com.venkat.a20180226_venkatnutalapati_nycschools.model.College;
import com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegeslist.CollegeListActivity;
import com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegeslist.list.CollegesAdapter;
import com.venkat.a20180226_venkatnutalapati_nycschools.util.SimpleDividerItemDecoration;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by venkat on 08/20/18.
 *
 * This class is responsible for inflating the view from layout and binds it to the activity
 */

public class CollegeView {

    @BindView(R.id.colleges_recycler_view)
    RecyclerView list;
    @BindView(R.id.loading_indicator)
    ProgressBar loading;

    View view;
    CollegesAdapter adapter;

    public CollegeView(CollegeListActivity context) {
        RelativeLayout parent = new RelativeLayout(context);
        parent.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));

        view  = LayoutInflater.from(context).inflate(R.layout.colleges_list_screen, parent, false);
        ButterKnife.bind(this, view);

        adapter = new CollegesAdapter();

        list.setAdapter(adapter);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);

        list.setLayoutManager(mLayoutManager);

        //adding the decorator item to recycler view for divider
        list.addItemDecoration(new SimpleDividerItemDecoration(context));
    }

    //Observer for list item click
    public Observable<Integer> itemClicks()
    {
        return adapter.observeClicks();
    }

    public void swapAdapter(ArrayList<College> collegeArray) {
        adapter.swapAdapter(collegeArray);
    }

    public View view() {
        return view;
    }
}
