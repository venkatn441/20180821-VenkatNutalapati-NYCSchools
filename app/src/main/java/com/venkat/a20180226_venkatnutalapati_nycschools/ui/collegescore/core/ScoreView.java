package com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegescore.core;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.venkat.a20180226_venkatnutalapati_nycschools.R;
import com.venkat.a20180226_venkatnutalapati_nycschools.model.Scores;
import com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegescore.ScoresActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by venkat on 08/20/18.
 *
 * View class is responsible for inflating the view from layout and binds it to the activity
 */

public class ScoreView extends AppCompatActivity{

    @BindView(R.id.school_name)
    TextView schoolName;
    @BindView(R.id.reading_score)
    TextView readingScore;
    @BindView(R.id.writing_score)
    TextView writingScore;
    @BindView(R.id.math_score)
    TextView mathScore;
    @BindView(R.id.number_of_students)
    TextView numberOfStudents;

    View view;

    public ScoreView(ScoresActivity context) {
        RelativeLayout parent = new RelativeLayout(context);
        parent.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));

        view  = LayoutInflater.from(context).inflate(R.layout.score_activity, parent, true);
        ButterKnife.bind(this, view);
    }

    public View view() {
        return view;
    }

    //Sets the response to the text feilds
    public void setScoreResult(Scores scoreReport) {
        schoolName.setText("School :"+scoreReport.getSchoolName());
        numberOfStudents.setText("Number of Test Takers:"+scoreReport.getNumOfSatTestTakers());
        readingScore.setText("Reading Average :"+scoreReport.getSatCriticalReadingAvgScore());
        writingScore.setText("Writing Average"+scoreReport.getSatWritingAvgScore());
        mathScore.setText("Math Average"+scoreReport.getSatMathAvgScore());
    }

    public void setNoScoreAvailable(){
        schoolName.setText("No socres available for the selected school");
    }
}
