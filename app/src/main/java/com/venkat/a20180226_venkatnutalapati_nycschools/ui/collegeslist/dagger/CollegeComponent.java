package com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegeslist.dagger;

import com.venkat.a20180226_venkatnutalapati_nycschools.application.builder.AppComponent;
import com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegeslist.CollegeListActivity;

import dagger.Component;

/**
 * Created by venkat on 08/20/18.
 *
 * Component is like an interface b/w the classes which require's dependencies and classes
 * which provide those dependecies.
 */

@CollegeScope
@Component(dependencies = {AppComponent.class} , modules = {CollegeModule.class})
public interface CollegeComponent {
    void inject(CollegeListActivity activity);
}
