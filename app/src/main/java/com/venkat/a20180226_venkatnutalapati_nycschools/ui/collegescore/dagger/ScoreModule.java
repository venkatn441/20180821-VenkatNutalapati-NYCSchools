package com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegescore.dagger;

import com.venkat.a20180226_venkatnutalapati_nycschools.api.NycOpenDataApi;
import com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegescore.ScoresActivity;
import com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegescore.core.ScoreModel;
import com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegescore.core.ScorePresenter;
import com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegescore.core.ScoreView;
import com.venkat.a20180226_venkatnutalapati_nycschools.util.rx.RxSchedulers;

import dagger.Module;
import dagger.Provides;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by venkat on 08/20/18.
 */

@Module
public class ScoreModule {
    ScoresActivity context;

    public ScoreModule(ScoresActivity context) {
        this.context = context;
    }

    @ScoreScope
    @Provides
    ScoreView providesView() {
        return new ScoreView(context);
    }

    @ScoreScope
    @Provides
    ScorePresenter providesPresenter(ScoreView view, ScoreModel model, RxSchedulers rxSchedulers) {
        CompositeSubscription subscription = new CompositeSubscription();
        return new ScorePresenter(view, model,rxSchedulers, subscription);
    }

    @ScoreScope
    @Provides
    ScoresActivity providesContext() {
        return context;
    }

    @ScoreScope
    @Provides
    ScoreModel provideModel(NycOpenDataApi api) {
        return new ScoreModel(context, api);
    }
}

