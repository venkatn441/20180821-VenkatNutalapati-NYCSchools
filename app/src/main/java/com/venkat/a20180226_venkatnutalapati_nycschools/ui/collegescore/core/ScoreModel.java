package com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegescore.core;

import com.venkat.a20180226_venkatnutalapati_nycschools.api.NycOpenDataApi;
import com.venkat.a20180226_venkatnutalapati_nycschools.model.Scores;
import com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegescore.ScoresActivity;
import com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegeslist.CollegeListActivity;
import com.venkat.a20180226_venkatnutalapati_nycschools.util.NetworkUtils;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by venkat on 08/20/18.
 *
 * Model class which provides methods to make network calls as observables.
 * Presenter class will subscribe for these observables
 */

public class ScoreModel {

    ScoresActivity context;
    NycOpenDataApi api;

    public ScoreModel(ScoresActivity context, NycOpenDataApi api) {
        this.context = context;
        this.api = api;
    }
    Observable<ArrayList<Scores>> getScoresForSelectedCollege(String dbn) {
        return api.getScores(dbn);
    }

    Observable<Boolean> isNetworkAvailable() {
        return NetworkUtils.isNetworkAvailableObservable(context);
    }
}
