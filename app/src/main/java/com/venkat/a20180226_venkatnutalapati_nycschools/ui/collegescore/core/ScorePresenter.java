package com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegescore.core;

import android.util.Log;
import android.view.View;

import com.venkat.a20180226_venkatnutalapati_nycschools.model.Scores;
import com.venkat.a20180226_venkatnutalapati_nycschools.util.rx.RxSchedulers;

import java.util.ArrayList;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

import static android.content.ContentValues.TAG;

/**
 * Created by venkat on 08/20/18.
 *
 * Presenter class converts observables from Model class to subscriptions.
 * User Interactions from View will be passed here and view is upated with response.
 */

public class ScorePresenter {
    ScoreView view;
    ScoreModel model;
    CompositeSubscription subscriptions;
    RxSchedulers rxSchedulers;
    Scores scoreReport;

    private static final String TAG = "ScorePresenter";

    public ScorePresenter(ScoreView view, ScoreModel model, RxSchedulers schedulers, CompositeSubscription subscriptions) {
        this.view = view;
        this.model = model;
        this.rxSchedulers = schedulers;
        this.subscriptions = subscriptions;
    }

    public void onCreate(String dbnCode) {
        Log.d(TAG, "presenter oncreate");
        subscriptions.add(getScores(dbnCode));
    }

    public void onDestroy() {
        subscriptions.clear();
    }

    //Checks for network availability and if network is available
    //a network call is made to get the scores for selected school.
    //If the scores are available view is updated with socres
    //or else "Scores not Available will be shown"
    private Subscription getScores(String dbnCode) {
        return model.isNetworkAvailable().doOnNext(isAvailable -> {
            if (!isAvailable) {
                Log.d(TAG, "no connection");
            }
        }).
                filter(isNetworkAvailable -> true).
                flatMap(isAvailable -> model.getScoresForSelectedCollege(dbnCode)).
                subscribeOn(rxSchedulers.io()).observeOn(rxSchedulers.androidThread()).
                subscribe(scoreResponse -> {
                    Log.d(TAG,"score response size"+scoreResponse.size());
                    if(scoreResponse.isEmpty()) {
                        view.setNoScoreAvailable();
                    } else {
                        scoreReport = scoreResponse.get(0);
                        view.setScoreResult(scoreReport);
                    }
                        }, throwable -> {
                            Log.d(TAG, "request failed :" + throwable.toString());
                        }
                );
    }

}
