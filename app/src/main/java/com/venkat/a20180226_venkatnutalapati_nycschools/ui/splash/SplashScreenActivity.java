package com.venkat.a20180226_venkatnutalapati_nycschools.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.venkat.a20180226_venkatnutalapati_nycschools.R;
import com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegeslist.CollegeListActivity;
import com.venkat.a20180226_venkatnutalapati_nycschools.util.Constants;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        LoadSchoolsForState("NY");
    }

    private void LoadSchoolsForState(String State) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent mainIntent = new Intent(SplashScreenActivity.this, CollegeListActivity.class);
                mainIntent.putExtra(Constants.STATE,"NY");
                SplashScreenActivity.this.startActivity(mainIntent);
                SplashScreenActivity.this.finish();
            }
        }, Constants.SPLASH_DISPLAY_LENGTH);
    }
}
