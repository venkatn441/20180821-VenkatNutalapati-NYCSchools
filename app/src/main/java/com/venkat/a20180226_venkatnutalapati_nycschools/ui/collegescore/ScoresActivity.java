package com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegescore;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.venkat.a20180226_venkatnutalapati_nycschools.application.AppController;
import com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegescore.core.ScorePresenter;
import com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegescore.core.ScoreView;
import com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegescore.dagger.DaggerScoreComponent;
import com.venkat.a20180226_venkatnutalapati_nycschools.ui.collegescore.dagger.ScoreModule;

import javax.inject.Inject;

/**
 * Created by venkat on 08/20/18.
 */

public class ScoresActivity extends AppCompatActivity{

    //Injecting dependencies using dagger
    @Inject
    ScoreView view;
    @Inject
    ScorePresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String dbnCode = null;
        DaggerScoreComponent.builder().appComponent(AppController.getNetComponent()).
                scoreModule(new ScoreModule(this)).build().inject(this);

        setContentView(view.view());

        if(getIntent() != null) {
            dbnCode = getIntent().getStringExtra("dbn");
        }
        presenter.onCreate(dbnCode);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }
}
