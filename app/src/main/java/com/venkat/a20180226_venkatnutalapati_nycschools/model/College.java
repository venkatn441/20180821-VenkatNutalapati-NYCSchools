package com.venkat.a20180226_venkatnutalapati_nycschools.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by venkat on 08/20/18.
 */

public class College {
        private String auditioninformation6;

        private String bbl;

        private String auditioninformation5;

        private String grade9geapplicants5;

        private String attendance_rate;

        private String auditioninformation4;

        private String phone_number;

        private String grade9geapplicants6;

        private String auditioninformation3;

        private String school_name;

        private String grade9geapplicants3;

        private String auditioninformation2;

        private String grade9geapplicants4;

        private String auditioninformation1;

        private String grade9geapplicants1;

        private String interest1;

        private String grade9geapplicants2;

        private String interest3;

        private String interest2;

        private String interest5;

        private String interest4;

        private String interest6;

        private String city;

        private String subway;

        private String grades2018;

        private String seats9ge3;

        private String seats9ge4;

        private String academicopportunities1;

        private String seats9ge1;

        private String seats9ge2;

        private String seats9ge5;

        private String seats9ge6;

        private String neighborhood;

        private String grade9swdapplicantsperseat2;

        private String grade9swdapplicantsperseat1;

        private String academicopportunities2;

        private String academicopportunities3;

        private String building_code;

        private String code6;

        private String code5;

        private String grade9gefilledflag6;

        private String code2;

        private String grade9gefilledflag4;

        private String code1;

        private String grade9gefilledflag5;

        private String code4;

        private String grade9gefilledflag2;

        private String code3;

        private String bin;

        private String grade9gefilledflag3;

        private String latitude;

        private String grade9gefilledflag1;

        private String boro;

        private String seats106;

        private String admissionspriority15;

        private String seats105;

        private String admissionspriority16;

        private String admissionspriority13;

        private String admissionspriority14;

        private String seats102;

        private String psal_sports_boys;

        private String seats101;

        private String seats9swd6;

        private String seats104;

        private String method6;

        private String seats9swd5;

        private String seats103;

        private String method5;

        private String method4;

        private String method3;

        private String method2;

        private String overview_paragraph;

        private String method1;

        private String school_accessibility_description;

        private String grade9swdfilledflag1;

        private String grade9swdfilledflag3;

        private String finalgrades;

        private String grade9swdfilledflag2;

        private String graduation_rate;

        private String admissionspriority12;

        private String grade9swdfilledflag5;

        private String admissionspriority11;

        private String grade9swdfilledflag4;

        private String grade9swdfilledflag6;

        private String fax_number;

        private String school_email;

        private String requirement4_1;

        private String requirement4_3;

        private String program2;

        private String requirement4_2;

        private String program3;

        private String requirement4_5;

        private String requirement4_4;

        private String program1;

        private String requirement4_6;

        private String borough;

        private String seats9swd1;

        private String seats9swd2;

        private String seats9swd3;

        private String extracurricular_activities;

        private String seats9swd4;

        private String program6;

        private String program5;

        private String program4;

        private String advancedplacement_courses;

        private String pct_stu_safe;

        private String bus;

        private String addtl_info1;

        private String location;

        private String shared_space;

        private String requirement2_6;

        private String requirement2_4;

        private String psal_sports_girls;

        private String requirement2_5;

        @SerializedName("total_students")
        private String totalstudents;

        private String requirement2_2;

        private String diplomaendorsements;

        private String requirement2_3;

        private String dbn;

        private String requirement2_1;

        private String requirement3_1;

        private String grade9geapplicantsperseat4;

        private String requirement3_2;

        private String grade9geapplicantsperseat3;

        private String grade9geapplicantsperseat2;

        private String longitude;

        private String grade9geapplicantsperseat1;

        private String requirement3_5;

        private String community_board;

        private String requirement3_6;

        private String census_tract;

        private String requirement3_3;

        private String grade9geapplicantsperseat6;

        private String requirement3_4;

        private String grade9geapplicantsperseat5;

        private String state_code;

        private String nta;

        private String prgdesc3;

        private String grade9swdapplicants4;

        private String prgdesc2;

        private String grade9swdapplicants3;

        private String prgdesc5;

        private String pct_stu_enough_variety;

        private String grade9swdapplicants6;

        private String prgdesc4;

        private String grade9swdapplicants5;

        private String prgdesc1;

        private String grade9swdapplicants2;

        private String grade9swdapplicants1;

        private String end_time;

        private String college_career_rate;

        private String council_district;

        private String grade9swdapplicantsperseat6;

        private String grade9swdapplicantsperseat5;

        private String grade9swdapplicantsperseat4;

        private String grade9swdapplicantsperseat3;

        private String prgdesc6;

        private String school_10th_seats;

        private String primary_address_line_1;

        private String requirement1_1;

        private String zip;

        private String requirement1_2;

        private String requirement1_3;

        private String requirement1_4;

        private String requirement1_5;

        private String website;

        private String requirement1_6;

        private String psal_sports_coed;

        private String ell_programs;

        private String start_time;

        private String language_classes;

        public String getAuditioninformation6 ()
        {
            return auditioninformation6;
        }

        public void setAuditioninformation6 (String auditioninformation6)
        {
            this.auditioninformation6 = auditioninformation6;
        }

        public String getBbl ()
        {
            return bbl;
        }

        public void setBbl (String bbl)
        {
            this.bbl = bbl;
        }

        public String getAuditioninformation5 ()
        {
            return auditioninformation5;
        }

        public void setAuditioninformation5 (String auditioninformation5)
        {
            this.auditioninformation5 = auditioninformation5;
        }

        public String getGrade9geapplicants5 ()
        {
            return grade9geapplicants5;
        }

        public void setGrade9geapplicants5 (String grade9geapplicants5)
        {
            this.grade9geapplicants5 = grade9geapplicants5;
        }

        public String getAttendance_rate ()
        {
            return attendance_rate;
        }

        public void setAttendance_rate (String attendance_rate)
        {
            this.attendance_rate = attendance_rate;
        }

        public String getAuditioninformation4 ()
        {
            return auditioninformation4;
        }

        public void setAuditioninformation4 (String auditioninformation4)
        {
            this.auditioninformation4 = auditioninformation4;
        }

        public String getPhone_number ()
        {
            return phone_number;
        }

        public void setPhone_number (String phone_number)
        {
            this.phone_number = phone_number;
        }

        public String getGrade9geapplicants6 ()
        {
            return grade9geapplicants6;
        }

        public void setGrade9geapplicants6 (String grade9geapplicants6)
        {
            this.grade9geapplicants6 = grade9geapplicants6;
        }

        public String getAuditioninformation3 ()
        {
            return auditioninformation3;
        }

        public void setAuditioninformation3 (String auditioninformation3)
        {
            this.auditioninformation3 = auditioninformation3;
        }

        public String getSchool_name ()
        {
            return school_name;
        }

        public void setSchool_name (String school_name)
        {
            this.school_name = school_name;
        }

        public String getGrade9geapplicants3 ()
        {
            return grade9geapplicants3;
        }

        public void setGrade9geapplicants3 (String grade9geapplicants3)
        {
            this.grade9geapplicants3 = grade9geapplicants3;
        }

        public String getAuditioninformation2 ()
        {
            return auditioninformation2;
        }

        public void setAuditioninformation2 (String auditioninformation2)
        {
            this.auditioninformation2 = auditioninformation2;
        }

        public String getGrade9geapplicants4 ()
        {
            return grade9geapplicants4;
        }

        public void setGrade9geapplicants4 (String grade9geapplicants4)
        {
            this.grade9geapplicants4 = grade9geapplicants4;
        }

        public String getAuditioninformation1 ()
        {
            return auditioninformation1;
        }

        public void setAuditioninformation1 (String auditioninformation1)
        {
            this.auditioninformation1 = auditioninformation1;
        }

        public String getGrade9geapplicants1 ()
        {
            return grade9geapplicants1;
        }

        public void setGrade9geapplicants1 (String grade9geapplicants1)
        {
            this.grade9geapplicants1 = grade9geapplicants1;
        }

        public String getInterest1 ()
        {
            return interest1;
        }

        public void setInterest1 (String interest1)
        {
            this.interest1 = interest1;
        }

        public String getGrade9geapplicants2 ()
        {
            return grade9geapplicants2;
        }

        public void setGrade9geapplicants2 (String grade9geapplicants2)
        {
            this.grade9geapplicants2 = grade9geapplicants2;
        }

        public String getInterest3 ()
        {
            return interest3;
        }

        public void setInterest3 (String interest3)
        {
            this.interest3 = interest3;
        }

        public String getInterest2 ()
        {
            return interest2;
        }

        public void setInterest2 (String interest2)
        {
            this.interest2 = interest2;
        }

        public String getInterest5 ()
        {
            return interest5;
        }

        public void setInterest5 (String interest5)
        {
            this.interest5 = interest5;
        }

        public String getInterest4 ()
        {
            return interest4;
        }

        public void setInterest4 (String interest4)
        {
            this.interest4 = interest4;
        }

        public String getInterest6 ()
        {
            return interest6;
        }

        public void setInterest6 (String interest6)
        {
            this.interest6 = interest6;
        }

        public String getCity ()
        {
            return city;
        }

        public void setCity (String city)
        {
            this.city = city;
        }

        public String getSubway ()
        {
            return subway;
        }

        public void setSubway (String subway)
        {
            this.subway = subway;
        }

        public String getGrades2018 ()
        {
            return grades2018;
        }

        public void setGrades2018 (String grades2018)
        {
            this.grades2018 = grades2018;
        }

        public String getSeats9ge3 ()
        {
            return seats9ge3;
        }

        public void setSeats9ge3 (String seats9ge3)
        {
            this.seats9ge3 = seats9ge3;
        }

        public String getSeats9ge4 ()
        {
            return seats9ge4;
        }

        public void setSeats9ge4 (String seats9ge4)
        {
            this.seats9ge4 = seats9ge4;
        }

        public String getAcademicopportunities1 ()
        {
            return academicopportunities1;
        }

        public void setAcademicopportunities1 (String academicopportunities1)
        {
            this.academicopportunities1 = academicopportunities1;
        }

        public String getSeats9ge1 ()
        {
            return seats9ge1;
        }

        public void setSeats9ge1 (String seats9ge1)
        {
            this.seats9ge1 = seats9ge1;
        }

        public String getSeats9ge2 ()
        {
            return seats9ge2;
        }

        public void setSeats9ge2 (String seats9ge2)
        {
            this.seats9ge2 = seats9ge2;
        }

        public String getSeats9ge5 ()
        {
            return seats9ge5;
        }

        public void setSeats9ge5 (String seats9ge5)
        {
            this.seats9ge5 = seats9ge5;
        }

        public String getSeats9ge6 ()
        {
            return seats9ge6;
        }

        public void setSeats9ge6 (String seats9ge6)
        {
            this.seats9ge6 = seats9ge6;
        }

        public String getNeighborhood ()
        {
            return neighborhood;
        }

        public void setNeighborhood (String neighborhood)
        {
            this.neighborhood = neighborhood;
        }

        public String getGrade9swdapplicantsperseat2 ()
        {
            return grade9swdapplicantsperseat2;
        }

        public void setGrade9swdapplicantsperseat2 (String grade9swdapplicantsperseat2)
        {
            this.grade9swdapplicantsperseat2 = grade9swdapplicantsperseat2;
        }

        public String getGrade9swdapplicantsperseat1 ()
        {
            return grade9swdapplicantsperseat1;
        }

        public void setGrade9swdapplicantsperseat1 (String grade9swdapplicantsperseat1)
        {
            this.grade9swdapplicantsperseat1 = grade9swdapplicantsperseat1;
        }

        public String getAcademicopportunities2 ()
        {
            return academicopportunities2;
        }

        public void setAcademicopportunities2 (String academicopportunities2)
        {
            this.academicopportunities2 = academicopportunities2;
        }

        public String getAcademicopportunities3 ()
        {
            return academicopportunities3;
        }

        public void setAcademicopportunities3 (String academicopportunities3)
        {
            this.academicopportunities3 = academicopportunities3;
        }

        public String getBuilding_code ()
        {
            return building_code;
        }

        public void setBuilding_code (String building_code)
        {
            this.building_code = building_code;
        }

        public String getCode6 ()
        {
            return code6;
        }

        public void setCode6 (String code6)
        {
            this.code6 = code6;
        }

        public String getCode5 ()
        {
            return code5;
        }

        public void setCode5 (String code5)
        {
            this.code5 = code5;
        }

        public String getGrade9gefilledflag6 ()
        {
            return grade9gefilledflag6;
        }

        public void setGrade9gefilledflag6 (String grade9gefilledflag6)
        {
            this.grade9gefilledflag6 = grade9gefilledflag6;
        }

        public String getCode2 ()
        {
            return code2;
        }

        public void setCode2 (String code2)
        {
            this.code2 = code2;
        }

        public String getGrade9gefilledflag4 ()
        {
            return grade9gefilledflag4;
        }

        public void setGrade9gefilledflag4 (String grade9gefilledflag4)
        {
            this.grade9gefilledflag4 = grade9gefilledflag4;
        }

        public String getCode1 ()
        {
            return code1;
        }

        public void setCode1 (String code1)
        {
            this.code1 = code1;
        }

        public String getGrade9gefilledflag5 ()
        {
            return grade9gefilledflag5;
        }

        public void setGrade9gefilledflag5 (String grade9gefilledflag5)
        {
            this.grade9gefilledflag5 = grade9gefilledflag5;
        }

        public String getCode4 ()
        {
            return code4;
        }

        public void setCode4 (String code4)
        {
            this.code4 = code4;
        }

        public String getGrade9gefilledflag2 ()
        {
            return grade9gefilledflag2;
        }

        public void setGrade9gefilledflag2 (String grade9gefilledflag2)
        {
            this.grade9gefilledflag2 = grade9gefilledflag2;
        }

        public String getCode3 ()
        {
            return code3;
        }

        public void setCode3 (String code3)
        {
            this.code3 = code3;
        }

        public String getBin ()
        {
            return bin;
        }

        public void setBin (String bin)
        {
            this.bin = bin;
        }

        public String getGrade9gefilledflag3 ()
        {
            return grade9gefilledflag3;
        }

        public void setGrade9gefilledflag3 (String grade9gefilledflag3)
        {
            this.grade9gefilledflag3 = grade9gefilledflag3;
        }

        public String getLatitude ()
        {
            return latitude;
        }

        public void setLatitude (String latitude)
        {
            this.latitude = latitude;
        }

        public String getGrade9gefilledflag1 ()
        {
            return grade9gefilledflag1;
        }

        public void setGrade9gefilledflag1 (String grade9gefilledflag1)
        {
            this.grade9gefilledflag1 = grade9gefilledflag1;
        }

        public String getBoro ()
        {
            return boro;
        }

        public void setBoro (String boro)
        {
            this.boro = boro;
        }

        public String getSeats106 ()
        {
            return seats106;
        }

        public void setSeats106 (String seats106)
        {
            this.seats106 = seats106;
        }

        public String getAdmissionspriority15 ()
        {
            return admissionspriority15;
        }

        public void setAdmissionspriority15 (String admissionspriority15)
        {
            this.admissionspriority15 = admissionspriority15;
        }

        public String getSeats105 ()
        {
            return seats105;
        }

        public void setSeats105 (String seats105)
        {
            this.seats105 = seats105;
        }

        public String getAdmissionspriority16 ()
        {
            return admissionspriority16;
        }

        public void setAdmissionspriority16 (String admissionspriority16)
        {
            this.admissionspriority16 = admissionspriority16;
        }

        public String getAdmissionspriority13 ()
        {
            return admissionspriority13;
        }

        public void setAdmissionspriority13 (String admissionspriority13)
        {
            this.admissionspriority13 = admissionspriority13;
        }

        public String getAdmissionspriority14 ()
        {
            return admissionspriority14;
        }

        public void setAdmissionspriority14 (String admissionspriority14)
        {
            this.admissionspriority14 = admissionspriority14;
        }

        public String getSeats102 ()
        {
            return seats102;
        }

        public void setSeats102 (String seats102)
        {
            this.seats102 = seats102;
        }

        public String getPsal_sports_boys ()
        {
            return psal_sports_boys;
        }

        public void setPsal_sports_boys (String psal_sports_boys)
        {
            this.psal_sports_boys = psal_sports_boys;
        }

        public String getSeats101 ()
        {
            return seats101;
        }

        public void setSeats101 (String seats101)
        {
            this.seats101 = seats101;
        }

        public String getSeats9swd6 ()
        {
            return seats9swd6;
        }

        public void setSeats9swd6 (String seats9swd6)
        {
            this.seats9swd6 = seats9swd6;
        }

        public String getSeats104 ()
        {
            return seats104;
        }

        public void setSeats104 (String seats104)
        {
            this.seats104 = seats104;
        }

        public String getMethod6 ()
        {
            return method6;
        }

        public void setMethod6 (String method6)
        {
            this.method6 = method6;
        }

        public String getSeats9swd5 ()
        {
            return seats9swd5;
        }

        public void setSeats9swd5 (String seats9swd5)
        {
            this.seats9swd5 = seats9swd5;
        }

        public String getSeats103 ()
        {
            return seats103;
        }

        public void setSeats103 (String seats103)
        {
            this.seats103 = seats103;
        }

        public String getMethod5 ()
        {
            return method5;
        }

        public void setMethod5 (String method5)
        {
            this.method5 = method5;
        }

        public String getMethod4 ()
        {
            return method4;
        }

        public void setMethod4 (String method4)
        {
            this.method4 = method4;
        }

        public String getMethod3 ()
        {
            return method3;
        }

        public void setMethod3 (String method3)
        {
            this.method3 = method3;
        }

        public String getMethod2 ()
        {
            return method2;
        }

        public void setMethod2 (String method2)
        {
            this.method2 = method2;
        }

        public String getOverview_paragraph ()
        {
            return overview_paragraph;
        }

        public void setOverview_paragraph (String overview_paragraph)
        {
            this.overview_paragraph = overview_paragraph;
        }

        public String getMethod1 ()
        {
            return method1;
        }

        public void setMethod1 (String method1)
        {
            this.method1 = method1;
        }

        public String getSchool_accessibility_description ()
        {
            return school_accessibility_description;
        }

        public void setSchool_accessibility_description (String school_accessibility_description)
        {
            this.school_accessibility_description = school_accessibility_description;
        }

        public String getGrade9swdfilledflag1 ()
        {
            return grade9swdfilledflag1;
        }

        public void setGrade9swdfilledflag1 (String grade9swdfilledflag1)
        {
            this.grade9swdfilledflag1 = grade9swdfilledflag1;
        }

        public String getGrade9swdfilledflag3 ()
        {
            return grade9swdfilledflag3;
        }

        public void setGrade9swdfilledflag3 (String grade9swdfilledflag3)
        {
            this.grade9swdfilledflag3 = grade9swdfilledflag3;
        }

        public String getFinalgrades ()
        {
            return finalgrades;
        }

        public void setFinalgrades (String finalgrades)
        {
            this.finalgrades = finalgrades;
        }

        public String getGrade9swdfilledflag2 ()
        {
            return grade9swdfilledflag2;
        }

        public void setGrade9swdfilledflag2 (String grade9swdfilledflag2)
        {
            this.grade9swdfilledflag2 = grade9swdfilledflag2;
        }

        public String getGraduation_rate ()
        {
            return graduation_rate;
        }

        public void setGraduation_rate (String graduation_rate)
        {
            this.graduation_rate = graduation_rate;
        }

        public String getAdmissionspriority12 ()
        {
            return admissionspriority12;
        }

        public void setAdmissionspriority12 (String admissionspriority12)
        {
            this.admissionspriority12 = admissionspriority12;
        }

        public String getGrade9swdfilledflag5 ()
        {
            return grade9swdfilledflag5;
        }

        public void setGrade9swdfilledflag5 (String grade9swdfilledflag5)
        {
            this.grade9swdfilledflag5 = grade9swdfilledflag5;
        }

        public String getAdmissionspriority11 ()
        {
            return admissionspriority11;
        }

        public void setAdmissionspriority11 (String admissionspriority11)
        {
            this.admissionspriority11 = admissionspriority11;
        }

        public String getGrade9swdfilledflag4 ()
        {
            return grade9swdfilledflag4;
        }

        public void setGrade9swdfilledflag4 (String grade9swdfilledflag4)
        {
            this.grade9swdfilledflag4 = grade9swdfilledflag4;
        }

        public String getGrade9swdfilledflag6 ()
        {
            return grade9swdfilledflag6;
        }

        public void setGrade9swdfilledflag6 (String grade9swdfilledflag6)
        {
            this.grade9swdfilledflag6 = grade9swdfilledflag6;
        }

        public String getFax_number ()
        {
            return fax_number;
        }

        public void setFax_number (String fax_number)
        {
            this.fax_number = fax_number;
        }

        public String getSchool_email ()
        {
            return school_email;
        }

        public void setSchool_email (String school_email)
        {
            this.school_email = school_email;
        }

        public String getRequirement4_1 ()
        {
            return requirement4_1;
        }

        public void setRequirement4_1 (String requirement4_1)
        {
            this.requirement4_1 = requirement4_1;
        }

        public String getRequirement4_3 ()
        {
            return requirement4_3;
        }

        public void setRequirement4_3 (String requirement4_3)
        {
            this.requirement4_3 = requirement4_3;
        }

        public String getProgram2 ()
        {
            return program2;
        }

        public void setProgram2 (String program2)
        {
            this.program2 = program2;
        }

        public String getRequirement4_2 ()
        {
            return requirement4_2;
        }

        public void setRequirement4_2 (String requirement4_2)
        {
            this.requirement4_2 = requirement4_2;
        }

        public String getProgram3 ()
        {
            return program3;
        }

        public void setProgram3 (String program3)
        {
            this.program3 = program3;
        }

        public String getRequirement4_5 ()
        {
            return requirement4_5;
        }

        public void setRequirement4_5 (String requirement4_5)
        {
            this.requirement4_5 = requirement4_5;
        }

        public String getRequirement4_4 ()
        {
            return requirement4_4;
        }

        public void setRequirement4_4 (String requirement4_4)
        {
            this.requirement4_4 = requirement4_4;
        }

        public String getProgram1 ()
        {
            return program1;
        }

        public void setProgram1 (String program1)
        {
            this.program1 = program1;
        }

        public String getRequirement4_6 ()
        {
            return requirement4_6;
        }

        public void setRequirement4_6 (String requirement4_6)
        {
            this.requirement4_6 = requirement4_6;
        }

        public String getBorough ()
        {
            return borough;
        }

        public void setBorough (String borough)
        {
            this.borough = borough;
        }

        public String getSeats9swd1 ()
        {
            return seats9swd1;
        }

        public void setSeats9swd1 (String seats9swd1)
        {
            this.seats9swd1 = seats9swd1;
        }

        public String getSeats9swd2 ()
        {
            return seats9swd2;
        }

        public void setSeats9swd2 (String seats9swd2)
        {
            this.seats9swd2 = seats9swd2;
        }

        public String getSeats9swd3 ()
        {
            return seats9swd3;
        }

        public void setSeats9swd3 (String seats9swd3)
        {
            this.seats9swd3 = seats9swd3;
        }

        public String getExtracurricular_activities ()
        {
            return extracurricular_activities;
        }

        public void setExtracurricular_activities (String extracurricular_activities)
        {
            this.extracurricular_activities = extracurricular_activities;
        }

        public String getSeats9swd4 ()
        {
            return seats9swd4;
        }

        public void setSeats9swd4 (String seats9swd4)
        {
            this.seats9swd4 = seats9swd4;
        }

        public String getProgram6 ()
        {
            return program6;
        }

        public void setProgram6 (String program6)
        {
            this.program6 = program6;
        }

        public String getProgram5 ()
        {
            return program5;
        }

        public void setProgram5 (String program5)
        {
            this.program5 = program5;
        }

        public String getProgram4 ()
        {
            return program4;
        }

        public void setProgram4 (String program4)
        {
            this.program4 = program4;
        }

        public String getAdvancedplacement_courses ()
        {
            return advancedplacement_courses;
        }

        public void setAdvancedplacement_courses (String advancedplacement_courses)
        {
            this.advancedplacement_courses = advancedplacement_courses;
        }

        public String getPct_stu_safe ()
        {
            return pct_stu_safe;
        }

        public void setPct_stu_safe (String pct_stu_safe)
        {
            this.pct_stu_safe = pct_stu_safe;
        }

        public String getBus ()
        {
            return bus;
        }

        public void setBus (String bus)
        {
            this.bus = bus;
        }

        public String getAddtl_info1 ()
        {
            return addtl_info1;
        }

        public void setAddtl_info1 (String addtl_info1)
        {
            this.addtl_info1 = addtl_info1;
        }

        public String getLocation ()
        {
            return location;
        }

        public void setLocation (String location)
        {
            this.location = location;
        }

        public String getShared_space ()
        {
            return shared_space;
        }

        public void setShared_space (String shared_space)
        {
            this.shared_space = shared_space;
        }

        public String getRequirement2_6 ()
        {
            return requirement2_6;
        }

        public void setRequirement2_6 (String requirement2_6)
        {
            this.requirement2_6 = requirement2_6;
        }

        public String getRequirement2_4 ()
        {
            return requirement2_4;
        }

        public void setRequirement2_4 (String requirement2_4)
        {
            this.requirement2_4 = requirement2_4;
        }

        public String getPsal_sports_girls ()
        {
            return psal_sports_girls;
        }

        public void setPsal_sports_girls (String psal_sports_girls)
        {
            this.psal_sports_girls = psal_sports_girls;
        }

        public String getRequirement2_5 ()
        {
            return requirement2_5;
        }

        public void setRequirement2_5 (String requirement2_5)
        {
            this.requirement2_5 = requirement2_5;
        }

        public String getTotalstudents ()
        {
            return totalstudents;
        }

        public void setTotalstudents (String totalstudents)
        {
            this.totalstudents = totalstudents;
        }

        public String getRequirement2_2 ()
        {
            return requirement2_2;
        }

        public void setRequirement2_2 (String requirement2_2)
        {
            this.requirement2_2 = requirement2_2;
        }

        public String getDiplomaendorsements ()
        {
            return diplomaendorsements;
        }

        public void setDiplomaendorsements (String diplomaendorsements)
        {
            this.diplomaendorsements = diplomaendorsements;
        }

        public String getRequirement2_3 ()
        {
            return requirement2_3;
        }

        public void setRequirement2_3 (String requirement2_3)
        {
            this.requirement2_3 = requirement2_3;
        }

        public String getDbn ()
        {
            return dbn;
        }

        public void setDbn (String dbn)
        {
            this.dbn = dbn;
        }

        public String getRequirement2_1 ()
        {
            return requirement2_1;
        }

        public void setRequirement2_1 (String requirement2_1)
        {
            this.requirement2_1 = requirement2_1;
        }

        public String getRequirement3_1 ()
        {
            return requirement3_1;
        }

        public void setRequirement3_1 (String requirement3_1)
        {
            this.requirement3_1 = requirement3_1;
        }

        public String getGrade9geapplicantsperseat4 ()
        {
            return grade9geapplicantsperseat4;
        }

        public void setGrade9geapplicantsperseat4 (String grade9geapplicantsperseat4)
        {
            this.grade9geapplicantsperseat4 = grade9geapplicantsperseat4;
        }

        public String getRequirement3_2 ()
        {
            return requirement3_2;
        }

        public void setRequirement3_2 (String requirement3_2)
        {
            this.requirement3_2 = requirement3_2;
        }

        public String getGrade9geapplicantsperseat3 ()
        {
            return grade9geapplicantsperseat3;
        }

        public void setGrade9geapplicantsperseat3 (String grade9geapplicantsperseat3)
        {
            this.grade9geapplicantsperseat3 = grade9geapplicantsperseat3;
        }

        public String getGrade9geapplicantsperseat2 ()
        {
            return grade9geapplicantsperseat2;
        }

        public void setGrade9geapplicantsperseat2 (String grade9geapplicantsperseat2)
        {
            this.grade9geapplicantsperseat2 = grade9geapplicantsperseat2;
        }

        public String getLongitude ()
        {
            return longitude;
        }

        public void setLongitude (String longitude)
        {
            this.longitude = longitude;
        }

        public String getGrade9geapplicantsperseat1 ()
        {
            return grade9geapplicantsperseat1;
        }

        public void setGrade9geapplicantsperseat1 (String grade9geapplicantsperseat1)
        {
            this.grade9geapplicantsperseat1 = grade9geapplicantsperseat1;
        }

        public String getRequirement3_5 ()
        {
            return requirement3_5;
        }

        public void setRequirement3_5 (String requirement3_5)
        {
            this.requirement3_5 = requirement3_5;
        }

        public String getCommunity_board ()
        {
            return community_board;
        }

        public void setCommunity_board (String community_board)
        {
            this.community_board = community_board;
        }

        public String getRequirement3_6 ()
        {
            return requirement3_6;
        }

        public void setRequirement3_6 (String requirement3_6)
        {
            this.requirement3_6 = requirement3_6;
        }

        public String getCensus_tract ()
        {
            return census_tract;
        }

        public void setCensus_tract (String census_tract)
        {
            this.census_tract = census_tract;
        }

        public String getRequirement3_3 ()
        {
            return requirement3_3;
        }

        public void setRequirement3_3 (String requirement3_3)
        {
            this.requirement3_3 = requirement3_3;
        }

        public String getGrade9geapplicantsperseat6 ()
        {
            return grade9geapplicantsperseat6;
        }

        public void setGrade9geapplicantsperseat6 (String grade9geapplicantsperseat6)
        {
            this.grade9geapplicantsperseat6 = grade9geapplicantsperseat6;
        }

        public String getRequirement3_4 ()
        {
            return requirement3_4;
        }

        public void setRequirement3_4 (String requirement3_4)
        {
            this.requirement3_4 = requirement3_4;
        }

        public String getGrade9geapplicantsperseat5 ()
        {
            return grade9geapplicantsperseat5;
        }

        public void setGrade9geapplicantsperseat5 (String grade9geapplicantsperseat5)
        {
            this.grade9geapplicantsperseat5 = grade9geapplicantsperseat5;
        }

        public String getState_code ()
        {
            return state_code;
        }

        public void setState_code (String state_code)
        {
            this.state_code = state_code;
        }

        public String getNta ()
        {
            return nta;
        }

        public void setNta (String nta)
        {
            this.nta = nta;
        }

        public String getPrgdesc3 ()
        {
            return prgdesc3;
        }

        public void setPrgdesc3 (String prgdesc3)
        {
            this.prgdesc3 = prgdesc3;
        }

        public String getGrade9swdapplicants4 ()
        {
            return grade9swdapplicants4;
        }

        public void setGrade9swdapplicants4 (String grade9swdapplicants4)
        {
            this.grade9swdapplicants4 = grade9swdapplicants4;
        }

        public String getPrgdesc2 ()
        {
            return prgdesc2;
        }

        public void setPrgdesc2 (String prgdesc2)
        {
            this.prgdesc2 = prgdesc2;
        }

        public String getGrade9swdapplicants3 ()
        {
            return grade9swdapplicants3;
        }

        public void setGrade9swdapplicants3 (String grade9swdapplicants3)
        {
            this.grade9swdapplicants3 = grade9swdapplicants3;
        }

        public String getPrgdesc5 ()
        {
            return prgdesc5;
        }

        public void setPrgdesc5 (String prgdesc5)
        {
            this.prgdesc5 = prgdesc5;
        }

        public String getPct_stu_enough_variety ()
        {
            return pct_stu_enough_variety;
        }

        public void setPct_stu_enough_variety (String pct_stu_enough_variety)
        {
            this.pct_stu_enough_variety = pct_stu_enough_variety;
        }

        public String getGrade9swdapplicants6 ()
        {
            return grade9swdapplicants6;
        }

        public void setGrade9swdapplicants6 (String grade9swdapplicants6)
        {
            this.grade9swdapplicants6 = grade9swdapplicants6;
        }

        public String getPrgdesc4 ()
        {
            return prgdesc4;
        }

        public void setPrgdesc4 (String prgdesc4)
        {
            this.prgdesc4 = prgdesc4;
        }

        public String getGrade9swdapplicants5 ()
        {
            return grade9swdapplicants5;
        }

        public void setGrade9swdapplicants5 (String grade9swdapplicants5)
        {
            this.grade9swdapplicants5 = grade9swdapplicants5;
        }

        public String getPrgdesc1 ()
        {
            return prgdesc1;
        }

        public void setPrgdesc1 (String prgdesc1)
        {
            this.prgdesc1 = prgdesc1;
        }

        public String getGrade9swdapplicants2 ()
        {
            return grade9swdapplicants2;
        }

        public void setGrade9swdapplicants2 (String grade9swdapplicants2)
        {
            this.grade9swdapplicants2 = grade9swdapplicants2;
        }

        public String getGrade9swdapplicants1 ()
        {
            return grade9swdapplicants1;
        }

        public void setGrade9swdapplicants1 (String grade9swdapplicants1)
        {
            this.grade9swdapplicants1 = grade9swdapplicants1;
        }

        public String getEnd_time ()
        {
            return end_time;
        }

        public void setEnd_time (String end_time)
        {
            this.end_time = end_time;
        }

        public String getCollege_career_rate ()
        {
            return college_career_rate;
        }

        public void setCollege_career_rate (String college_career_rate)
        {
            this.college_career_rate = college_career_rate;
        }

        public String getCouncil_district ()
        {
            return council_district;
        }

        public void setCouncil_district (String council_district)
        {
            this.council_district = council_district;
        }

        public String getGrade9swdapplicantsperseat6 ()
        {
            return grade9swdapplicantsperseat6;
        }

        public void setGrade9swdapplicantsperseat6 (String grade9swdapplicantsperseat6)
        {
            this.grade9swdapplicantsperseat6 = grade9swdapplicantsperseat6;
        }

        public String getGrade9swdapplicantsperseat5 ()
        {
            return grade9swdapplicantsperseat5;
        }

        public void setGrade9swdapplicantsperseat5 (String grade9swdapplicantsperseat5)
        {
            this.grade9swdapplicantsperseat5 = grade9swdapplicantsperseat5;
        }

        public String getGrade9swdapplicantsperseat4 ()
        {
            return grade9swdapplicantsperseat4;
        }

        public void setGrade9swdapplicantsperseat4 (String grade9swdapplicantsperseat4)
        {
            this.grade9swdapplicantsperseat4 = grade9swdapplicantsperseat4;
        }

        public String getGrade9swdapplicantsperseat3 ()
        {
            return grade9swdapplicantsperseat3;
        }

        public void setGrade9swdapplicantsperseat3 (String grade9swdapplicantsperseat3)
        {
            this.grade9swdapplicantsperseat3 = grade9swdapplicantsperseat3;
        }

        public String getPrgdesc6 ()
        {
            return prgdesc6;
        }

        public void setPrgdesc6 (String prgdesc6)
        {
            this.prgdesc6 = prgdesc6;
        }

        public String getSchool_10th_seats ()
        {
            return school_10th_seats;
        }

        public void setSchool_10th_seats (String school_10th_seats)
        {
            this.school_10th_seats = school_10th_seats;
        }

        public String getPrimary_address_line_1 ()
        {
            return primary_address_line_1;
        }

        public void setPrimary_address_line_1 (String primary_address_line_1)
        {
            this.primary_address_line_1 = primary_address_line_1;
        }

        public String getRequirement1_1 ()
        {
            return requirement1_1;
        }

        public void setRequirement1_1 (String requirement1_1)
        {
            this.requirement1_1 = requirement1_1;
        }

        public String getZip ()
        {
            return zip;
        }

        public void setZip (String zip)
        {
            this.zip = zip;
        }

        public String getRequirement1_2 ()
        {
            return requirement1_2;
        }

        public void setRequirement1_2 (String requirement1_2)
        {
            this.requirement1_2 = requirement1_2;
        }

        public String getRequirement1_3 ()
        {
            return requirement1_3;
        }

        public void setRequirement1_3 (String requirement1_3)
        {
            this.requirement1_3 = requirement1_3;
        }

        public String getRequirement1_4 ()
        {
            return requirement1_4;
        }

        public void setRequirement1_4 (String requirement1_4)
        {
            this.requirement1_4 = requirement1_4;
        }

        public String getRequirement1_5 ()
        {
            return requirement1_5;
        }

        public void setRequirement1_5 (String requirement1_5)
        {
            this.requirement1_5 = requirement1_5;
        }

        public String getWebsite ()
        {
            return website;
        }

        public void setWebsite (String website)
        {
            this.website = website;
        }

        public String getRequirement1_6 ()
        {
            return requirement1_6;
        }

        public void setRequirement1_6 (String requirement1_6)
        {
            this.requirement1_6 = requirement1_6;
        }

        public String getPsal_sports_coed ()
        {
            return psal_sports_coed;
        }

        public void setPsal_sports_coed (String psal_sports_coed)
        {
            this.psal_sports_coed = psal_sports_coed;
        }

        public String getEll_programs ()
        {
            return ell_programs;
        }

        public void setEll_programs (String ell_programs)
        {
            this.ell_programs = ell_programs;
        }

        public String getStart_time ()
        {
            return start_time;
        }

        public void setStart_time (String start_time)
        {
            this.start_time = start_time;
        }

        public String getLanguage_classes ()
        {
            return language_classes;
        }

        public void setLanguage_classes (String language_classes)
        {
            this.language_classes = language_classes;
        }
}

